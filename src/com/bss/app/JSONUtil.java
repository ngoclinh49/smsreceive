package com.bss.app;

import java.util.ArrayList;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


/**
 * Support funcs to try parse JsonObject
 */
public class JSONUtil {

	/**
	 * Default value when parsing JSONObject
	 */
	public static class JSONDefaultValue {
		public static final int INT = -1;
		public static final boolean BOOLEAN = false;
		public static final String STRING = "";
		public static final double DOUBLE = -1.0f;
		public static final JSONObject JSON_OBJECT = null;
		public static final JSONArray JSON_ARRAY = null;
	}

	/**
	 * Try to get 'int' value of json object. If exception has occurred, default
	 * value will be returned.
	 * 
	 * @param jsonObj
	 * @param key
	 * @return
	 */
	public static int intTryGetValue(JSONObject jsonObj, String key) {
		int value = JSONDefaultValue.INT;
		if (jsonObj.isNull(key)) // kiểm tra xem key có trong Object jsonObj hay
									// không ?
			return value; // nếu không có thì trả v�? giá trị mặc định

		try {
			value = jsonObj.getInt(key); // nếu không thì trả lại giá trị theo
											// int
		} catch (JSONException e) {
			ALog.e("JSONUtil", "intTryGetValue exception!");
			e.printStackTrace();
		}

		return value;
	}

	/**
	 * Try to get 'string' value of json object. If exception has occurred,
	 * default value will be returned.
	 * 
	 * @param jsonObj
	 * @param key
	 * @return
	 */
	public static String stringTryGetValue(JSONObject jsonObj, String key) {
		String value = JSONDefaultValue.STRING;

		// check if value of this key is null - tương tự với int String cũng vậy
		if (jsonObj.isNull(key))
			return value;

		try {
			value = jsonObj.getString(key);
		} catch (JSONException e) {
			ALog.e("JSONUtil", "stringTryGetValue exception!");
			ALog.e("Key=" + key, jsonObj.toString());
			e.printStackTrace();
		}

		return value;
	}


	/**
	 * Try to get 'double' value of json object. If exception has occurred,
	 * default value will be returned.
	 * 
	 * @param jsonObj
	 * @param key
	 * @return
	 */
	public static double doubleTryGetValue(JSONObject jsonObj, String key) {
		double value = JSONDefaultValue.INT;

		// check if value of this key is null
		if (jsonObj.isNull(key))
			return value;

		try {
			value = jsonObj.getDouble(key);
		} catch (JSONException e) {
			ALog.e("JSONUtil", "doubleTryGetValue exception!");
			ALog.e("Key=" + key, jsonObj.toString());
			e.printStackTrace();
		}

		return value;
	}

	/**
	 * Try to get 'boolean' value of json object. If exception has occurred,
	 * default value will be returned.
	 * 
	 * @param jsonObj
	 * @param key
	 * @return
	 */
	public static boolean booleanTryGetValue(JSONObject jsonObj, String key) {
		boolean value = JSONDefaultValue.BOOLEAN;

		// check if value of this key is null
		if (jsonObj.isNull(key))
			return value;

		try {
			value = jsonObj.getBoolean(key);
		} catch (JSONException e) {
			ALog.e("JSONUtil", "doubleTryGetValue exception!");
			ALog.e("Key=" + key, jsonObj.toString());
			e.printStackTrace();
		}

		return value;
	}

	/**
	 * Try to get 'json object' value of json object. If exception has occurred,
	 * default value will be returned.
	 * 
	 * tìm ra giá trị con của object json xem có json object nào nữa hay không
	 * 
	 * @param jsonObj
	 * @param key
	 * @return
	 */
	public static JSONObject jsonObjectTryGetValue(JSONObject jsonObj, String key) {
		JSONObject value = JSONDefaultValue.JSON_OBJECT;

		// check if value of this key is null
		if (jsonObj.isNull(key))
			return value;

		try {
			value = jsonObj.getJSONObject(key);
		} catch (JSONException e) {
			ALog.e("JSONUtil", "jsonObjectTryGetValue exception!");
			ALog.e("Key=" + key, jsonObj.toString());
			e.printStackTrace();
		}

		return value;
	}

	/**
	 * Try to get 'json array' value of json object. If exception has occurred,
	 * default value will be returned.
	 * 
	 * @param jsonObj
	 * @param key
	 * @return
	 */
	public static ArrayList<JSONObject> jsonArrayTryGetValue(JSONObject jsonObj, String key) {
		JSONArray array = JSONDefaultValue.JSON_ARRAY;
		ArrayList<JSONObject> value = new ArrayList<JSONObject>();

		// check if value of this key is null
		if (jsonObj.isNull(key))
			return value;

		try {
			array = jsonObj.getJSONArray(key);
			for (int i = 0; i < array.length(); i++) {
				value.add(array.getJSONObject(i));
			}
		} catch (JSONException e) {
			ALog.e("JSONUtil", "jsonArrayTryGetValue exception!");
			ALog.e("Key=" + key, jsonObj.toString());
			e.printStackTrace();
		}

		return value;
	}



}
