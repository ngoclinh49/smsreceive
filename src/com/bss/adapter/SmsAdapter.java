package com.bss.adapter;

import java.util.List;

import com.bss.receivesms.MainActivity;
import com.bss.receivesms.R;
import com.bss.smsreceive.entity.ContactEntity;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class SmsAdapter extends ArrayAdapter<ContactEntity> {

	private final List<ContactEntity> list;
	private final Activity context;

	public SmsAdapter(Activity context, List<ContactEntity> list) {
		super(context, R.layout.sms_item, list);
		this.context = context;
		this.list = list;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return super.getCount();
	}

	@Override
	public ContactEntity getItem(int position) {
		// TODO Auto-generated method stub
		return super.getItem(position);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = null;
		if (convertView == null) {
			LayoutInflater inflator = context.getLayoutInflater();
			view = inflator.inflate(R.layout.sms_item, null);
			final ViewHolder viewHolder = new ViewHolder();
			viewHolder.number = (TextView) view.findViewById(R.id.number);
			viewHolder.sms = (TextView) view.findViewById(R.id.sms);
			viewHolder.status = (TextView) view.findViewById(R.id.status);

			viewHolder.status.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					ContactEntity element = (ContactEntity) viewHolder.status.getTag();
					if(element.getStatus().equals("0"))
						((MainActivity)context).resendSms(element);
				}
			});

			view.setTag(viewHolder);
			viewHolder.status.setTag(list.get(position));
		} else {
			view = convertView;
			((ViewHolder) view.getTag()).status.setTag(list.get(position));
		}

		ViewHolder holder = (ViewHolder) view.getTag();
		holder.number.setText(list.get(position).getPhone());
		holder.sms.setText(list.get(position).getContent());

		if(list.get(position).getStatus().equals("1"))
			holder.status.setText("Sent");
		else
			holder.status.setText("Send error!");

		return view;
	}

	static class ViewHolder {
		protected TextView number;
		protected TextView sms;
		protected TextView status;
	}
} 