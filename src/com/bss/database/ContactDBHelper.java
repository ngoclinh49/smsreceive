package com.bss.database;

import java.util.ArrayList;

import com.bss.smsreceive.entity.ContactEntity;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class ContactDBHelper {
	// Database fields
	private SQLiteDatabase database;
	private MySQLiteHelper dbHelper;
	private String[] allColumns = { MySQLiteHelper.CONTACTS_COLUMN_ID, MySQLiteHelper.CONTACTS_COLUMN_PHONE,
			MySQLiteHelper.CONTACTS_COLUMN_SMS_CONTENT, MySQLiteHelper.CONTACTS_COLUMN_SMS_STATUS};

	public ContactDBHelper(Context context) {
		dbHelper = new MySQLiteHelper(context);
	}

	public void open() throws SQLException {
		database = dbHelper.getWritableDatabase();
	}

	public void close() {
		dbHelper.close();
	}

	public boolean insertContact  ( String phone, String content, String status)
	{
		ContentValues contentValues = new ContentValues();
		contentValues.put(MySQLiteHelper.CONTACTS_COLUMN_PHONE, phone);
		contentValues.put(MySQLiteHelper.CONTACTS_COLUMN_SMS_CONTENT, content);
		contentValues.put(MySQLiteHelper.CONTACTS_COLUMN_SMS_STATUS, status);
		database.insert(MySQLiteHelper.CONTACTS_TABLE_NAME, null, contentValues);

		return true;
	}

	public int getLastId() {
		int lastId =0;
		String query = "SELECT id from contacts order by id DESC limit 1";
		Cursor c = database.rawQuery (query,null);
		if (c != null && c.moveToFirst()) 
		{
			lastId = c.getInt(0);
			//The 0 is the column index, we only have 1 column, so the index is 0
		}
		return lastId;
	}

	public Cursor getData(int id){
		//		SQLiteDatabase db = this.getReadableDatabase();
		Cursor res =  database.rawQuery( "select * from contacts where id="+id+"", null );
		return res;
	}

	public int numberOfRows(){
		int numRows = (int) DatabaseUtils.queryNumEntries(database, MySQLiteHelper.CONTACTS_TABLE_NAME);
		return numRows;
	}

	public boolean updateContact (Integer id, String phone, String content, String status)
	{
		ContentValues contentValues = new ContentValues();
		contentValues.put(MySQLiteHelper.CONTACTS_COLUMN_PHONE, phone);
		contentValues.put(MySQLiteHelper.CONTACTS_COLUMN_SMS_CONTENT, content);
		contentValues.put(MySQLiteHelper.CONTACTS_COLUMN_SMS_STATUS, status);
		database.update("contacts", contentValues, "id = ? ", new String[] { Integer.toString(id) } );
		return true;
	}
	
	public boolean updateContactSendSMSStatus (int id, String status)
	{
		ContentValues contentValues = new ContentValues();
		contentValues.put(MySQLiteHelper.CONTACTS_COLUMN_SMS_STATUS, status);
		database.update("contacts", contentValues, "id = ? ", new String[] { Integer.toString(id) } );
		return true;
	}

	public Integer deleteContact (Integer id)
	{
		return database.delete("contacts", 
				"id = ? ", 
				new String[] { Integer.toString(id) });
	}
	public ArrayList<ContactEntity> getAllCotacts()
	{
		ArrayList<ContactEntity> array_list = new ArrayList<ContactEntity>();
		Cursor res =  database.rawQuery( "select * from contacts", null );
		res.moveToFirst();
		while(res.isAfterLast() == false){
			ContactEntity item = new ContactEntity();
			item.setId(res.getInt(res.getColumnIndex(MySQLiteHelper.CONTACTS_COLUMN_ID)));
			item.setPhone(res.getString(res.getColumnIndex(MySQLiteHelper.CONTACTS_COLUMN_PHONE)));
			item.setContent(res.getString(res.getColumnIndex(MySQLiteHelper.CONTACTS_COLUMN_SMS_CONTENT)));
			item.setStatus(res.getString(res.getColumnIndex(MySQLiteHelper.CONTACTS_COLUMN_SMS_STATUS)));
			array_list.add(item);
			res.moveToNext();
		}
		return array_list;
	}

}
