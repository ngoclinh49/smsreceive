package com.bss.database;

import java.util.HashMap;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class MySQLiteHelper  extends SQLiteOpenHelper{
	public static final String DATABASE_NAME = "sms_receive.db";
	public static final int DATABASE_VERSION = 1;
	public static final String CONTACTS_TABLE_NAME = "contacts";

	public static final String CONTACTS_COLUMN_ID = "id";
	public static final String CONTACTS_COLUMN_PHONE = "phone";
	public static final String CONTACTS_COLUMN_SMS_CONTENT = "sms_content";
	public static final String CONTACTS_COLUMN_SMS_STATUS = "status";

	private HashMap hp;

	private static final String DB_CONTACT_CREATE = "create table contacts " + "(id integer primary key, phone text,sms_content text, status text)";
	
	public MySQLiteHelper(Context context)
	{
	    super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		db.execSQL(DB_CONTACT_CREATE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		Log.w(MySQLiteHelper.class.getName(),
		        "Upgrading database from version " + oldVersion + " to "
		            + newVersion + ", which will destroy all old data");
		db.execSQL("DROP TABLE IF EXISTS "+CONTACTS_TABLE_NAME);
		onCreate(db);
	}

}
