package com.bss.customvolley;

import org.json.JSONObject;

public interface CallBackVolley {
	/**
	 * response request override
	 * 
	 * @param nameApi
	 * @param response
	 * @param messageError
	 */
	public void onApiResults(String nameApi, String response, String messageError);
}
