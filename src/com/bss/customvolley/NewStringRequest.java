package com.bss.customvolley;



import java.net.URLEncoder;
import java.util.Iterator;
import java.util.Map;

import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bss.app.AppController;
import com.bss.receivesms.R;

public class NewStringRequest{

	StringRequest request;
	private CallBackVolley callBack;
	private String url;
	private ProgressDialog progressDialog = null;
	private boolean isShowProcessDialog = false;

	private Listener<String> success = new Listener<String>() {

		@Override
		public void onResponse(String response) {
			Log.i("request", response);
			try {
				if(response != null){
					if(isShowProcessDialog == true && progressDialog !=null && progressDialog.isShowing())
						progressDialog.dismiss();
					callBack.onApiResults(url,response,null);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	};

	private ErrorListener error = new ErrorListener() {

		@Override
		public void onErrorResponse(VolleyError e) {
			e.printStackTrace();
			if(isShowProcessDialog == true && progressDialog!=null && progressDialog.isShowing())
				progressDialog.dismiss();
			callBack.onApiResults(url,null,e.getMessage());
			
		}
	};

	@SuppressWarnings({ "rawtypes", "deprecation" })
	public NewStringRequest(Context mcontext,int method, String url, final Map<String, String> params, CallBackVolley callback) {
		try {
			if(mcontext != null){
				if(isShowProcessDialog ==true && progressDialog == null){
					progressDialog = new ProgressDialog(mcontext);
					progressDialog.show();
					progressDialog.setCancelable(false);
					progressDialog.setContentView(R.layout.progressdialog);
				}
				if(progressDialog!=null &&!progressDialog.isShowing()) progressDialog.show();
			}else
				isShowProcessDialog = false;

			this.callBack = callback;
			this.url = url;

			if(method == Request.Method.GET){
				url += "?";
				Iterator iterator = params.entrySet().iterator();
				while (iterator.hasNext()) {
					Map.Entry mapEntry = (Map.Entry) iterator.next();
					url += "&"+mapEntry.getKey()+"="+ URLEncoder.encode(mapEntry.getValue().toString());
				}

				Log.d("Url Api", url);
				request = new StringRequest(method, url, success, error);
			}else if(method == Request.Method.POST){
				Log.d("Url Api", url);
				request = new StringRequest(method, url, success, error){

					@Override
					protected Map<String, String> getParams() throws AuthFailureError {
						return params;
					}

				};
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public NewStringRequest(Context mcontext,int method, String url, CallBackVolley callback) {
		this.callBack = callback;
		this.url = url;
		if(mcontext != null){
			if(isShowProcessDialog==true&&progressDialog == null){
				progressDialog = new ProgressDialog(mcontext);
				progressDialog.show();
				progressDialog.setCancelable(false);
				progressDialog.setContentView(R.layout.progressdialog);
			}
			if(progressDialog!=null &&!progressDialog.isShowing()) progressDialog.show();
		}else
			isShowProcessDialog = false;
		request = new StringRequest(method, url, success, error);
	}

	public boolean isShowProcessDialog() {
		return isShowProcessDialog;
	}

	public void setShowProcessDialog(boolean isShowProcessDialog) {
		this.isShowProcessDialog = isShowProcessDialog;
	}

	public NewStringRequest(String url, CallBackVolley callback) {
		this.callBack = callback;
		this.url = url;
		request = new StringRequest(url, success, error);
	}

	public void run(){
		AppController.getInstance().addToRequestQueue(request);
	}

	public void runWithCache(boolean isUse){
		request.setShouldCache(isUse);
		AppController.getInstance().addToRequestQueue(request);
	}

}
