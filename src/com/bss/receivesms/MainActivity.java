package com.bss.receivesms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.android.volley.Request;
import com.bss.adapter.SmsAdapter;
import com.bss.customvolley.CallBackVolley;
import com.bss.customvolley.NewStringRequest;
import com.bss.database.ContactDBHelper;
import com.bss.helper.PreferenceHelpers;
import com.bss.receivesms.ServiceCommunicator.ServiceCommunicatorBinder;
import com.bss.smsreceive.entity.ContactEntity;
import com.kpbird.chipsedittextlibrary.ChipsMultiAutoCompleteTextview;

import android.support.v7.app.ActionBarActivity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

public class MainActivity extends ActionBarActivity implements CallBackVolley {

	boolean mServiceBound = false;
	ServiceCommunicator mServiceCommunicator;

	ChipsMultiAutoCompleteTextview ch;
	EditText edtUrl;
	ContactDBHelper contactDB;
	SmsAdapter adapter;
	ArrayList<ContactEntity> listContact;
	ListView listMess;
	int id;
	
	private SMSreceiver mSMSreceiver;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// input phone number
		ch= (ChipsMultiAutoCompleteTextview) findViewById(R.id.chipsMultiAutoCompleteTextview1);
		edtUrl = (EditText)findViewById(R.id.edtInPutUrl);

		String num = PreferenceHelpers.getPreference(MainActivity.this, "list_num");
		String url = PreferenceHelpers.getPreference(MainActivity.this, "url");

		if(num!=null){
			ch.setText(num);
			ch.setChips();
		}
		
		if(url!=null)
			edtUrl.setText(url);

		// button save
		Button btn = (Button)findViewById(R.id.tvTitleRight);
		btn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				String listNum = ch.getText().toString().trim();
				String url = edtUrl.getText().toString().trim();

				PreferenceHelpers.setPreference(MainActivity.this, "list_num", listNum);
				PreferenceHelpers.setPreference(MainActivity.this, "url", url);
				((TextView)findViewById(R.id.mes_notif)).setVisibility(View.VISIBLE);//show text green
			}
		});


		// connect to databse
		contactDB = new ContactDBHelper(this);
		contactDB.open();

		// import data demo
//		contactDB.insertContact("0978373216", "anh nho em", "0");
//		contactDB.insertContact("0978373216", "anh nho em", "1");
//		contactDB.insertContact("0978373216", "anh nho em", "1");
//		contactDB.insertContact("0978373216", "anh nho em", "1");
//		contactDB.insertContact("0978373216", "anh nho em", "0");
//		contactDB.insertContact("0978373216", "anh nho em", "1");

		// get all data in database
		listContact = contactDB.getAllCotacts();

		// show list mess sent
		listMess = (ListView)findViewById(R.id.list);
		SmsAdapter adapter = new SmsAdapter(this, listContact);
		listMess.setAdapter(adapter);

		mSMSreceiver = new SMSreceiver();
		IntentFilter mIntentFilter = new IntentFilter();
		mIntentFilter.addAction("android.provider.Telephony.SMS_RECEIVED");
		registerReceiver(mSMSreceiver, mIntentFilter);
		
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
//		unbindService(mServiceConnection);
	}

	/**
	 * Resend sms
	 * @param ContactEntity item
	 */
	public void resendSms(ContactEntity item) {
		id = item.getId();
		String url = PreferenceHelpers.getPreference(MainActivity.this, "url");
		Map<String, String> params = new HashMap<String,String>();
		params.put("no",item.getPhone());
		params.put("txt",item.getContent());
		NewStringRequest request = new NewStringRequest(this,Request.Method.GET, url,this);
		request.setShowProcessDialog(false);
		request.runWithCache(false);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

//	private ServiceConnection mServiceConnection = new ServiceConnection() {
//
//		@Override
//		public void onServiceDisconnected(ComponentName name) {
//			mServiceBound = false;
//		}
//
//		@Override
//		public void onServiceConnected(ComponentName name, IBinder service) {
//			ServiceCommunicatorBinder myBinder = (ServiceCommunicatorBinder) service;
//			mServiceCommunicator = myBinder.getService();
//			mServiceBound = true;
//		}
//	};

	@Override
	public void onApiResults(String nameApi, String response, String messageError) {
		if(response != null && !response.equals("")){
			if(response.equals("1"))
				contactDB.updateContactSendSMSStatus(id,response);

			listContact = contactDB.getAllCotacts();
			adapter.notifyDataSetChanged();
		} 
	}
}
