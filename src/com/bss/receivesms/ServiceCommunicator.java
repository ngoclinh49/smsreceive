package com.bss.receivesms;

import java.util.HashMap;
import java.util.Map;

import com.android.volley.Request;
import com.bss.customvolley.CallBackVolley;
import com.bss.customvolley.NewStringRequest;
import com.bss.database.ContactDBHelper;
import com.bss.helper.PreferenceHelpers;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

public class ServiceCommunicator extends Service implements CallBackVolley
{
	private final String TAG = this.getClass().getSimpleName();
	ContactDBHelper contactDB;
	int lastId = 0;


	String noNumber;
	String content;

	@Override
	public void onCreate()
	{
		super.onCreate();
		contactDB = new ContactDBHelper(this);
		contactDB.open();

	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		
		String url = PreferenceHelpers.getPreference(this, "url");
		String noNumber=intent.getStringExtra("no");
		String content=intent.getStringExtra("content");
		
		// call api upload message
		requestUpSMS( noNumber,content,  url);
		
		return super.onStartCommand(intent, flags, startId);
	}

//	
	@Override
	public void onDestroy()
	{
		super.onDestroy();
	}

	public void requestUpSMS(String number, String smsText, String url) {
		contactDB.insertContact(number, smsText, "0");
		lastId = contactDB.getLastId();
		Map<String, String> params = new HashMap<String,String>();
		params.put("no",number);
		params.put("txt",smsText);
		NewStringRequest request = new NewStringRequest(this,Request.Method.GET, url,params,this);
		request.setShowProcessDialog(false);
		request.runWithCache(false);
	}

	@Override
	public IBinder onBind(Intent intent) {
		return new ServiceCommunicatorBinder();
	}

	public class ServiceCommunicatorBinder extends Binder {
		public ServiceCommunicator getService() {
			return ServiceCommunicator.this;
		}
	}

	@Override
	public void onApiResults(String nameApi, String response, String messageError) {
		if(response != null && !response.equals("")){
			if(response.equals("1"))
				contactDB.updateContactSendSMSStatus(lastId,response);
		} 
		this.stopSelf();
	}
}
