package com.bss.receivesms;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;

import com.bss.database.ContactDBHelper;
import com.bss.helper.PreferenceHelpers;

public class SMSreceiver extends BroadcastReceiver
{
	private final String TAG = this.getClass().getSimpleName();
	ContactDBHelper contactDB;
	int lastId = 0;
//	Context mContext;
//	public SMSreceiver(Context context) {
//		mContext = context;
//	}

	@Override
	public void onReceive(Context context, Intent intent)
	{
		Log.i(TAG, "BroadcastReceiver");

		String listNum = PreferenceHelpers.getPreference(context, "list_num");
		String url = PreferenceHelpers.getPreference(context, "url");

		if(listNum!=null){
			String[] arrNum = listNum.split(",");
			Bundle extras = intent.getExtras();
			String strMessage = "";
			if ( extras != null )
			{
				Object[] smsextras = (Object[]) extras.get( "pdus" );
				for ( int i = 0; i < smsextras.length; i++ )
				{
					SmsMessage smsmsg = SmsMessage.createFromPdu((byte[])smsextras[i]);
					String strMsgBody = smsmsg.getMessageBody().toString();
					String strMsgSrc = smsmsg.getOriginatingAddress();
					strMessage += "SMS from " + strMsgSrc + " : " + strMsgBody;                    
					Log.i(TAG, strMessage);
				
					for (int j = 0; j < arrNum.length; j++) {
						if(strMsgSrc.contains(arrNum[j])){
							// start service up sms
							Intent newIntent = new Intent(context, ServiceCommunicator.class);
							newIntent.putExtra("no", strMsgSrc);
							newIntent.putExtra("content", strMsgBody);
							context.startService(newIntent); 
						}
					}
				}
			}
		}
	}
}
